// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Tile.generated.h"

//To bind in GameMode
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerEnter);

UCLASS()
class TESTINGGROUNDS_API ATile : public AActor
{
	GENERATED_BODY()
public:
	// Sets default values for this actor's properties
	ATile();
	FTransform GetAttachLocation() const;
	class ANavMeshBoundsVolume* GetNavVolume() const;
	void SetNavVolume(ANavMeshBoundsVolume* Volume);
	FPlayerEnter PlayerEntered;
	TArray<AActor*> GetSpawnedActors() const;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditAnywhere)
	UBoxComponent* BoundsVolume;
	UPROPERTY(EditAnywhere)
	UArrowComponent* AttachPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Barrier")
	UStaticMeshComponent* Barrier;
	UPROPERTY(EditAnywhere)
	UMaterialInterface* BarrierMaterial;
	UPROPERTY()
	UMaterialInstanceDynamic* DynamicMaterial;
	UPROPERTY(VisibleDefaultsOnly)
	UStaticMeshComponent* Floor;
	UPROPERTY(VisibleDefaultsOnly)
	class UGrassComponent* Grass01;
	UPROPERTY(VisibleDefaultsOnly)
	class UGrassComponent* Grass02;
	UPROPERTY(VisibleDefaultsOnly)
	class UGrassComponent* Grass03;

	UFUNCTION(BlueprintImplementableEvent, Category = "Tile")
	void OnEnter();
	UFUNCTION(BlueprintImplementableEvent, Category = "Tile")
	void OnExit();

	UFUNCTION(BlueprintCallable, Category = "Tile")
	void UpdateBarrierMaterial(float Blend, float Opacity);
	UFUNCTION(BlueprintCallable, Category = "Spawn", meta = (DisplayName = "Spawn AI in Tile"))
	void SpawnAIInTile(TSubclassOf<APawn> SpawnActor, float Radius = 100.f, bool bOnlyYaw = true, int32 MinSpawn = 0, int32 MaxSpawn = 3);
	UFUNCTION(BlueprintCallable, Category = "Spawn")
	void SpawnActorsInTile(TSubclassOf<AActor> SpawnActor, float Radius = 100.f, bool bOnlyYaw = true, int32 MinSpawn = 0, int32 MaxSpawn = 3, float MinScale = 1, float MaxScale = 1);
	UFUNCTION()
	void OnBoundsBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnBoundsEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	UFUNCTION()
	void OnSpawnedActorDestroyed(AActor* DestroyedActor);
private:
	bool bConquered;
	USceneComponent* SharedRoot;
	class ANavMeshBoundsVolume* NavVolume;
	TArray<AActor*> SpawnedActors;
};
