// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FoliageInstancedStaticMeshComponent.h"
#include "GrassComponent.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API UGrassComponent : public UFoliageInstancedStaticMeshComponent
{
	GENERATED_BODY()
public:
	void SpawnGrass(const FBox& Extents);
	UGrassComponent();
	UPROPERTY(EditDefaultsOnly, Category = "Grass Component")
	int32 SpawnCount;
};
