// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Gun.generated.h"

class USoundCue;

UENUM()
enum class EWeaponState : uint8
{
	Idle,
	Firing,
	//Not sure if you want these two
	Reloading,
	Equipping
	////
};

UENUM()
enum class EAmmoType : uint8
{
	EBullet,
	ERocket
};

USTRUCT()
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

		//Infinite ammo for reloads 
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	bool bInfiniteAmmo;

	//Infinite ammo in clip
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	bool bInfiniteClip;

	//Max ammo
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	int32 MaxAmmo;

	//Clip size
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	int32 AmmoPerClip;

	//Initial clips 
	UPROPERTY(EditDefaultsOnly, Category = "Ammo")
	int32 InitialClips;

	//Time between two consecutive shots
	UPROPERTY(EditDefaultsOnly, Category = "WeaponStat")
	float TimeBetweenShots;


	//Defaults
	FWeaponData()
	{
		bInfiniteAmmo = false;
		bInfiniteClip = false;
		MaxAmmo = 100;
		AmmoPerClip = 20;
		InitialClips = 4;
		TimeBetweenShots = 0.2f;
	}
};

USTRUCT()
struct FWeaponAnim
{
	GENERATED_USTRUCT_BODY()

	//Animation played on FP Pawn
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	class UAnimMontage* Pawn1P;

	//Animation played on TP Pawn
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	class UAnimMontage* Pawn3P;
};

UCLASS(Abstract, Blueprintable)
class TESTINGGROUNDS_API AGun : public AActor
{
	GENERATED_BODY()

	//Initial setup
	virtual void PostInitializeComponents() override;
	//sets up attachment to FP or TP
	void SetupAttachment();

	//////////////////////////////////////////////////
	//Ammo

	//Give weapon ammo, currently not used
	void GiveAmmo(int32 Amount);
	//Use a bullet/projectile
	void UseAmmo();
	//Get ammo type for this weapon, default to bullet
	virtual EAmmoType GetAmmoType() const { return EAmmoType::EBullet; }


	//////////////////////////////////////////////////
	//Control
	bool CanFire() const;
	//bool CanReload() const;

public:
	//////////////////////////////////////////////////
	//Data reading

	//Weapon/ammo
	EWeaponState GetCurrentState() const;
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	int32 GetCurrentAmmo() const;
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	int32 GetCurrentAmmoInClip() const;
	int32 GetAmmoPerClip() const;
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	int32 GetMaxAmmo() const;
	bool HasInfiniteAmmo() const;
	bool HasInfiniteClip() const;

	//Mesh
	USkeletalMeshComponent* GetWeaponMesh() const;
	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
	class AShooterCharacter* GetCharOwner() const;

protected:

	//Pawn that owns this weapon
	class AShooterCharacter* OwningChar;
	//Weapon config data
	UPROPERTY(EditDefaultsOnly, Category = "Config")
	FWeaponData WeaponConfig;

	//////////////////////////////////////////////////
	//Sound variables

	//Used to fade out the sound if looping fire sound 
	UAudioComponent* FireAC;
	//Single fire sound
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* FireSound;
	//Looped fire sound
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* FireLoopSound;
	//Finished burst sound if looping fire
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	USoundCue* FireFinishSound;

	////Out of ammo sound
	//UPROPERTY(EditDefaultsOnly, Category = "Sound")
	//USoundCue* OutOfAmmoSound;
	////Reload sound
	//UPROPERTY(EditDefaultsOnly, Category = "Sound")
	//USoundCue* ReloadSound;

	//////////////////////////////////////////////////
	//Effects & animation variables

	//Name of socket for muzzle in weapon mesh
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	FName MuzzleAttachPoint;
	//FX for muzzle flash
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* MuzzleFX;
	//Spawned component for muzzle FX
	UParticleSystemComponent* MuzzlePSC;

	//Camera shake on firing
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	TSubclassOf<UCameraShake> FireCameraShake;
	//Force feedback effect to play when the weapon is fired
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UForceFeedbackEffect* FireForceFeedback;

	//AnimMontages to play each time we fire
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	FWeaponAnim FireAnim;

	//////////////////////////////////////////////////
	//Flag variables

	// is muzzle FX looped?
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	bool bLoopedMuzzleFX;
	// is fire sound looped?
	UPROPERTY(EditDefaultsOnly, Category = "Sound")
	bool bLoopedFireSound;
	//is fire animation looped?
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
	bool bLoopedFireAnim;
	//is fire animation playing?
	bool bPlayingFireAnim;
	//is weapon fire active?
	bool bWantsToFire;
	//is reload animation playing?
	//bool bPendingReload;
	//weapon is refiring
	bool bRefiring;

	//////////////////////////////////////////////////
	//States

	//current weapon state
	EWeaponState CurrentState;
	//time of last successful weapon fire
	float LastFireTime;
	//last time when this weapon was switched to
	//float EquipStartedTime;
	//how much time weapon needs to be equipped
	//float EquipDuration;
	//current ammo
	int32 CurrentAmmo;
	//current ammo - inside clip
	int32 CurrentAmmoInClip;

	//////////////////////////////////////////////////
	//Timers

	//Handle management of HandleFiring timer
	FTimerHandle Timer_HandleFiring;
	//////////////////////////////////////////////////
	//Effects
	//start firing effects
	virtual void SimulateWeaponFire();
	//stop firing effects
	virtual void StopSimulateWeaponFire();

	//////////////////////////////////////////////////
	//Weapon usage

	//weapon specific fire implementation
	virtual void FireWeapon() PURE_VIRTUAL(AShooterWeapon::FireWeapon, );
	//update weapon state
	void SetWeaponState(EWeaponState NewState);
	//determine current weapon state 
	void DetermineWeaponState();
	//firing started
	virtual void OnBurstStarted();
	//firing finished
	virtual void OnBurstFinished();
	//handle weapon fire
	void HandleFiring();

	//////////////////////////////////////////////////
	//Weapon usage helpers

	//play weapon sounds
	UAudioComponent* PlayWeaponSound(USoundCue* Sound);
	//play weapon animations
	float PlayWeaponAnimation(const FWeaponAnim& Animation);
	//stop playing weapon animations
	void StopWeaponAnimation(const FWeaponAnim& Animation);
	//get the aim of the weapon, allowing for adjustments to be made by the weapon 
	virtual FVector GetAdjustedAim() const;
	//get the aim of the camera
	FVector GetCameraAim() const;
	//get the originating location for camera damage
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;
	//get the muzzle location of the weapon
	FVector GetMuzzleLocation() const;
	//get direction of weapon's muzzle
	FVector GetMuzzleDirection() const;
	//find hit
	FHitResult WeaponTrace(const FVector& TraceFrom, const FVector& TraceTo) const;

private:
	//Weapon mesh
	UPROPERTY(VisibleDefaultsOnly, Category = "Mesh")
	USkeletalMeshComponent* GunMesh;
public:
	// Sets default values for this actor's properties	
	AGun();
	//Set owner of weapon
	void SetOwningPawn(AShooterCharacter* Character);

	//////////////////////////////////////////////////
	//Input

	//Start weapon fire
	virtual void OnStartFire();
	//Stop weapon fire
	virtual void OnStopFire();
	//Start weapon reload
	//virtual void OnStartReload();
	//Stop weapon reload
	//virtual void OnStopReload();
	//Reload weapon
	//virtual void ReloadWeapon();
};