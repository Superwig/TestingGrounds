// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "TestingGroundsGameState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTileCountIncremented);

UCLASS()
class TESTINGGROUNDS_API ATestingGroundsGameState : public AGameState
{
	GENERATED_BODY()
	
public:
	ATestingGroundsGameState();
	UPROPERTY(BlueprintAssignable, Category = "UIEvent")
	FTileCountIncremented TileCountUpdated;

	UFUNCTION(BlueprintCallable)
	int32 GetTilesConquered() const;
	void IncrementTilesConquered();
private:
	int32 TilesConquered;	
};
