// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Gun.h"
#include "GunImpactEffects.h"
#include "GameFramework/DamageType.h"
#include "HitscanGun.generated.h"

USTRUCT()
struct FInstantWeaponData
{
	GENERATED_USTRUCT_BODY()

	//Weapon base spread (degrees)
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float WeaponSpread;

	//Aiming spread modifier
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float AimingSpreadMod;

	//Continuous firing: spread increment 
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float FiringSpreadIncrement;

	//Continuous firing: max increment
	UPROPERTY(EditDefaultsOnly, Category = "Accuracy")
	float FiringSpreadMax;

	//Weapon range 
	UPROPERTY(EditDefaultsOnly, Category = "WeaponStat")
	float WeaponRange;

	//Damage amount
	UPROPERTY(EditDefaultsOnly, Category = "WeaponStat")
	int32 HitDamage;

	//Type of damage
	UPROPERTY(EditDefaultsOnly, Category = "WeaponStat")
	TSubclassOf<UDamageType> DamageType;

	//Defaults 
	FInstantWeaponData()
	{
		WeaponSpread = 5.0f;
		AimingSpreadMod = 0.25f;
		FiringSpreadIncrement = 1.0f;
		FiringSpreadMax = 10.0f;
		WeaponRange = 10000.0f;
		HitDamage = 10;
		DamageType = UDamageType::StaticClass();
	}
};

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API AHitscanGun : public AGun
{
	GENERATED_BODY()
	
public:
	void SpawnTrailEffect(const FVector& EndPoint);
	void SpawnImpactEffects(const FHitResult& Impact);
protected:

	//Hitscan weapon config
	UPROPERTY(EditDefaultsOnly, Category = "Config")
	FInstantWeaponData InstantConfig;
	//Impact effects
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	TSubclassOf<AGunImpactEffects> ImpactTemplate;
	//Smoke trail
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	UParticleSystem* TrailFX;
	//Param name for beam target in smoke trail
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	FName TrailTargetParam;

	//Current spread from continuous firing
	float CurrentFiringSpread;
	float GetCurrentSpread() const;

	void ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir);
	void DealDamage(const FHitResult& Impact, const FVector& ShootDir);

	virtual void FireWeapon() override;
	
	
};
