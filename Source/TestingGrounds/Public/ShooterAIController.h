// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "ShooterAIController.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API AShooterAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	AShooterAIController();
	virtual void Possess(class APawn* InPawn) override;
	virtual void UnPossess() override;

	void SetEnemy(APawn* InPawn);
	class AShooterCharacter* GetEnemy() const;

protected:
	int32 EnemyKeyID;
private:
	class UBehaviorTreeComponent* BehaviorComp;
	UBlackboardComponent* BlackboardComp;
	
};
