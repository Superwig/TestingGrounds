// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "TestingGroundsGameMode.generated.h"
#define DEFAULT_MAX 3

UCLASS(minimalapi)
class ATestingGroundsGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATestingGroundsGameMode();
protected:
	virtual void BeginPlay() override;
	UFUNCTION()
	virtual void SpawnTile();
	UPROPERTY(EditDefaultsOnly, meta = (ClampMin = "3", UIMin = "3")) //Clamp default value to min 3.
	uint8 MaxTiles = DEFAULT_MAX;
private:
	FTransform AttachLocation;
	TCircularQueue<class ATile*> Tiles{ DEFAULT_MAX };
	TCircularQueue<class ANavMeshBoundsVolume*> NavVolumes{ DEFAULT_MAX };
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class ATile> TileClass;
};



