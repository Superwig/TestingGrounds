// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#ifndef __TESTINGGROUNDS_H__
#define __TESTINGGROUNDS_H__

#include "EngineMinimal.h"

constexpr ECollisionChannel ECC_Projectile = ECollisionChannel::ECC_GameTraceChannel1;
constexpr ECollisionChannel ECC_Weapon = ECollisionChannel::ECC_GameTraceChannel2;
constexpr ECollisionChannel ECC_Ground = ECollisionChannel::ECC_GameTraceChannel3;
constexpr ECollisionChannel ECC_Spawn = ECollisionChannel::ECC_GameTraceChannel4;

#endif
