// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerState.h"
#include "ShooterPlayerState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEnemiesKilled);

UCLASS()
class TESTINGGROUNDS_API AShooterPlayerState : public APlayerState
{
	GENERATED_BODY()	
public:
	int32 GetBulletsFired() const;
	UFUNCTION(BlueprintCallable)
	int32 GetEnemiesKilled() const;
	void AddBulletsFired(int32 NumBullets);
	void AddEnemiesKilled(int32 NumEnemies);
	UPROPERTY(BlueprintAssignable, Category = "UIEvent")
	FEnemiesKilled KillsUpdated;
private:
	int32 BulletsFired;
	int32 EnemiesKilled;
	
};
