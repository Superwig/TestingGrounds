// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ShooterCharacter.h"
#include "ShooterAI.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API AShooterAI : public AShooterCharacter
{
	GENERATED_BODY()
	friend class AShooterAIController;
public:
	AShooterAI();

private:
	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* AIBehavior;

	virtual bool IsFirstPerson() const override;

	virtual void FaceRotation(FRotator NewRotation, float DeltaTime = 0.f) override;
	
	
};
