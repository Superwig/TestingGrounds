// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "AIController.h"
#include "PatrollingComponent.h"
#include "FindNextWaypoint.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UFindNextWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	APawn* Pawn = OwnerComp.GetAIOwner()->GetPawn();
	UPatrollingComponent* PatrolComp = Pawn->FindComponentByClass<UPatrollingComponent>();
	if (!PatrolComp)
	{
		return EBTNodeResult::Failed;
	}
	AActor* PatrolPoint = PatrolComp->GetPatrolPoint();
	if (PatrolPoint == nullptr)
	{
		return EBTNodeResult::Failed;
	}
	Blackboard->SetValueAsObject(WaypointKey.SelectedKeyName, PatrolPoint);
	PatrolComp->Increment();
	return EBTNodeResult::Succeeded;
}
