// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "ShooterAI.h"
#include "ShooterAIController.h"

AShooterAI::AShooterAI()
{
	AIControllerClass = AShooterAIController::StaticClass();
	bUseControllerRotationYaw = true;
	Health = MaxHealth = 50.f;
}

bool AShooterAI::IsFirstPerson() const 
{
	return false;
}

void AShooterAI::FaceRotation(FRotator NewRotation, float DeltaTime)
{
	FRotator CurrentRotation = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, 8.0f);
	Super::FaceRotation(CurrentRotation, DeltaTime);
}