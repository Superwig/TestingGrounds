// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "ShooterPlayerState.h"


int32 AShooterPlayerState::GetBulletsFired() const
{
	return BulletsFired;
}

int32 AShooterPlayerState::GetEnemiesKilled() const
{
	return EnemiesKilled;
}

void AShooterPlayerState::AddBulletsFired(int32 NumBullets)
{
	BulletsFired += NumBullets;
}

void AShooterPlayerState::AddEnemiesKilled(int32 NumEnemies)
{
	EnemiesKilled += NumEnemies;
	KillsUpdated.Broadcast();
}

