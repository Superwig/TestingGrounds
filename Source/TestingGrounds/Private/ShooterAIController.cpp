// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "ShooterAI.h"
#include "ShooterAIController.h"
#include "ShooterCharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

AShooterAIController::AShooterAIController()
{
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehavoirComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	BrainComponent = BehaviorComp;
}

void AShooterAIController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);

	AShooterAI* AI = Cast<AShooterAI>(InPawn);
	// start behavior
	if (AI && AI->AIBehavior)
	{
		if (AI->AIBehavior->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*AI->AIBehavior->BlackboardAsset);
		}
		EnemyKeyID = BlackboardComp->GetKeyID("Enemy");
		BehaviorComp->StartTree(*(AI->AIBehavior));
	}
}

void AShooterAIController::UnPossess()
{
	Super::UnPossess();
	BehaviorComp->StopTree();
}


void AShooterAIController::SetEnemy(APawn* InPawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValue<UBlackboardKeyType_Object>(EnemyKeyID, InPawn);
		SetFocus(InPawn);
	}	
}

AShooterCharacter* AShooterAIController::GetEnemy() const 
{
	if (BlackboardComp)
	{
		return Cast<AShooterCharacter>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(EnemyKeyID));
	}
	return nullptr;
}