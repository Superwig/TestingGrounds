#include "TestingGrounds.h"
#include "Gun.h"
#include "ShooterCharacter.h"
#include "Sound/SoundCue.h"
#include "AIController.h"
#include "ShooterPlayerState.h"


//////////////////////////////////////////////////
//Setup
//////////////////////////////////////////////////

AGun::AGun()
{
	// Create a gun mesh component
	GunMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Gun"));
	GunMesh->bCastDynamicShadow = false;
	GunMesh->CastShadow = false;
	RootComponent = GunMesh;

	bLoopedMuzzleFX = false;
	bLoopedFireAnim = false;
	bPlayingFireAnim = false;
	bWantsToFire = false;
	CurrentState = EWeaponState::Idle;

	CurrentAmmo = 0;
	CurrentAmmoInClip = 0;
	LastFireTime = 0.0f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;

}


void AGun::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	//Give initial ammo
	if (WeaponConfig.InitialClips > 0)
	{
		CurrentAmmoInClip = WeaponConfig.AmmoPerClip;
		CurrentAmmo = WeaponConfig.AmmoPerClip * WeaponConfig.InitialClips;
	}
}
void AGun::SetOwningPawn(AShooterCharacter* Char)
{
	OwningChar = Char;
	//Actor responsible for damage caused by this actor
	Instigator = Char;
	SetOwner(Char);
	SetupAttachment();
}
void AGun::SetupAttachment()
{
	if (OwningChar)
	{
		//Attach to FirstPerson if player
		if (OwningChar->IsPlayerControlled())
		{
			USkeletalMeshComponent* PawnMesh1P = OwningChar->GetFPMesh();
			GunMesh->SetHiddenInGame(false);
			GunMesh->SetOnlyOwnerSee(true);
			GunMesh->AttachToComponent(PawnMesh1P, FAttachmentTransformRules::KeepRelativeTransform, TEXT("GripPoint"));
		}
		//Attach to ThirdPerson if AI
		else
		{
			USkeletalMeshComponent* UsePawnMesh = OwningChar->GetPawnMesh();
			GunMesh->AttachToComponent(UsePawnMesh, FAttachmentTransformRules::KeepRelativeTransform, TEXT("GripPoint"));
			GunMesh->SetHiddenInGame(false);
		}
	}
}

//////////////////////////////////////////////////
//Getters
//////////////////////////////////////////////////
bool AGun::CanFire() const
{
	bool bCanFire = OwningChar && OwningChar->IsAlive();
	bool bStateOKToFire = CurrentState == EWeaponState::Idle || CurrentState == EWeaponState::Firing;
	return bCanFire && bStateOKToFire;
}

EWeaponState AGun::GetCurrentState() const { return CurrentState; }
int32 AGun::GetCurrentAmmo() const { return CurrentAmmo; }
int32 AGun::GetCurrentAmmoInClip() const { return CurrentAmmoInClip; }
int32 AGun::GetAmmoPerClip() const { return WeaponConfig.AmmoPerClip; }
int32 AGun::GetMaxAmmo() const { return WeaponConfig.MaxAmmo; }
bool AGun::HasInfiniteAmmo() const { return WeaponConfig.bInfiniteAmmo; }
bool AGun::HasInfiniteClip() const { return WeaponConfig.bInfiniteClip; }
USkeletalMeshComponent* AGun::GetWeaponMesh() const { return GunMesh; }
AShooterCharacter* AGun::GetCharOwner() const { return OwningChar; }
FVector AGun::GetMuzzleLocation() const { return GunMesh->GetSocketLocation(MuzzleAttachPoint); }
FVector AGun::GetMuzzleDirection() const { return GunMesh->GetSocketRotation(MuzzleAttachPoint).Vector(); }

//////////////////////////////////////////////////
//Ammo
//////////////////////////////////////////////////

void AGun::GiveAmmo(int32 Amount)
{
	const int32 MissingAmmo = FMath::Max(0, WeaponConfig.MaxAmmo - CurrentAmmo);
	Amount = FMath::Min(Amount, MissingAmmo);
	CurrentAmmo += Amount;
}

void AGun::UseAmmo()
{
	if (!WeaponConfig.bInfiniteAmmo)
	{
		CurrentAmmoInClip--;
	}

	if (!WeaponConfig.bInfiniteAmmo && !WeaponConfig.bInfiniteClip)
	{
		CurrentAmmo--;
	}
	//Add to stats using PlayerState
	AController* OwningPawnPlayerController = OwningChar->Controller;
	if (OwningPawnPlayerController)
	{
		AShooterPlayerState* PlayerState = Cast<AShooterPlayerState>(OwningPawnPlayerController->PlayerState);
		switch (GetAmmoType())
		{
		case EAmmoType::EBullet:
		default:
			PlayerState->AddBulletsFired(1);
			break;
		}
	}
}

//////////////////////////////////////////////////
//Weapon Firing
//////////////////////////////////////////////////

void AGun::OnStartFire()
{
	if (!bWantsToFire)
	{
		bWantsToFire = true;
		DetermineWeaponState();
	}
}

void AGun::OnStopFire()
{
	if (bWantsToFire)
	{
		bWantsToFire = false;
		DetermineWeaponState();
	}
}

void AGun::DetermineWeaponState()
{
	//Set a default new state
	EWeaponState NewState = EWeaponState::Idle;
	//if wants to fire and can fire
	if (bWantsToFire && CanFire())
	{
		NewState = EWeaponState::Firing;
	}

	SetWeaponState(NewState); //Set the new state
}

void AGun::SetWeaponState(EWeaponState NewState)
{
	//Save current state
	const EWeaponState PrevState = CurrentState;
	//Stopped firing
	if (PrevState == EWeaponState::Firing && NewState != EWeaponState::Firing)
	{
		OnBurstFinished();
	}
	//Start Firing
	CurrentState = NewState;
	if (PrevState != EWeaponState::Firing && NewState == EWeaponState::Firing)
	{
		OnBurstStarted();
	}
}

void AGun::OnBurstStarted()
{
	//Get current time
	const float GameTime = GetWorld()->GetTimeSeconds();
	//Determine if it's too soon to shoot
	bool bTooSoon = LastFireTime > 0 && WeaponConfig.TimeBetweenShots > 0.0f && LastFireTime + WeaponConfig.TimeBetweenShots > GameTime;
	if (bTooSoon)
	{
		//if not set a timer to call HandleFiring when ready
		GetWorldTimerManager().SetTimer(Timer_HandleFiring, this, &AGun::HandleFiring, LastFireTime + WeaponConfig.TimeBetweenShots - GameTime, false);
	}
	else
	{
		HandleFiring();
	}
}

void AGun::OnBurstFinished()
{
	//stop firing FX and clear timer
	StopSimulateWeaponFire();
	GetWorldTimerManager().ClearTimer(Timer_HandleFiring);
}

void AGun::HandleFiring()
{
	//If can fire and has ammo or infinite ammo
	if ((CurrentAmmoInClip > 0 || WeaponConfig.bInfiniteClip || WeaponConfig.bInfiniteAmmo) && CanFire())
	{
		//play animation and effects
		SimulateWeaponFire();
		FireWeapon();
		UseAmmo();
	}
	//else if (CanReload())
	//{
	//	StartReload();
	//}
	else if (GetCurrentAmmo() == 0 && !bRefiring)
	{
		////Out of Ammo
		//PlayWeaponSound(OutOfAmmoSound);
		//AShooterPlayerController* MyPC = Cast<AShooterPlayerController>(MyPawn->Controller);
		//AShooterHUD* MyHUD = MyPC ? Cast<AShooterHUD>(MyPC->GetHUD()) : nullptr;
		//if (MyHUD)
		//{
		//	MyHUD->NotifyOutOfAmmo();
		//}
	}

	////reload after firing last round
	//if (CurrentAmmoInClip <= 0 && CanReload())
	//{
	//	StartReload();
	//}
	//Set up refire timer
	bRefiring = (CurrentState == EWeaponState::Firing && WeaponConfig.TimeBetweenShots > 0.0f);
	if (bRefiring)
	{
		GetWorldTimerManager().SetTimer(Timer_HandleFiring, this, &AGun::HandleFiring, WeaponConfig.TimeBetweenShots, false);
	}
	LastFireTime = GetWorld()->GetTimeSeconds();
}

//////////////////////////////////////////////////
//Effects
//////////////////////////////////////////////////

void AGun::SimulateWeaponFire()
{
	//Exit if not firing
	if (CurrentState != EWeaponState::Firing)
	{
		return;
	}
	//Has MuzzleFX
	if (MuzzleFX)
	{
		if (!bLoopedMuzzleFX || !MuzzlePSC)
		{	
			if (OwningChar && OwningChar->IsPlayerControlled())
			{
				MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, GunMesh, MuzzleAttachPoint);
				MuzzlePSC->bOwnerNoSee = false;
				MuzzlePSC->bOnlyOwnerSee = true;
			}
			else
			{
				MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, GunMesh, MuzzleAttachPoint);
			}
		}
	}

	if (!bLoopedFireAnim || !bPlayingFireAnim)
	{
		PlayWeaponAnimation(FireAnim);
		bPlayingFireAnim = true;
	}

	if (bLoopedFireSound)
	{
		if (!FireAC)
		{
			FireAC = PlayWeaponSound(FireLoopSound);
		}
	}
	else
	{
		PlayWeaponSound(FireSound);
	}
	APlayerController* PC = OwningChar ? Cast<APlayerController>(OwningChar->Controller) : nullptr;
	//If player controlled
	if (PC)
	{
		if (FireCameraShake)
		{
			PC->ClientPlayCameraShake(FireCameraShake, 1);
		}
		if (FireForceFeedback)
		{
			PC->ClientPlayForceFeedback(FireForceFeedback, false, "Weapon");
		}
	}
}

void AGun::StopSimulateWeaponFire()
{
	if (bLoopedMuzzleFX)
	{
		if (MuzzlePSC)
		{
			MuzzlePSC->DeactivateSystem();
			MuzzlePSC = nullptr;
		}
	}

	if (bLoopedFireAnim && bPlayingFireAnim)
	{
		StopWeaponAnimation(FireAnim);
		bPlayingFireAnim = false;
	}

	if (FireAC)
	{
		FireAC->FadeOut(0.1f, 0.0f);
		FireAC = nullptr;

		PlayWeaponSound(FireFinishSound);
	}
}

//////////////////////////////////////////////////
//Effect Helpers
//////////////////////////////////////////////////

UAudioComponent* AGun::PlayWeaponSound(USoundCue* Sound)
{
	UAudioComponent* AC = nullptr;
	if (Sound && OwningChar)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, OwningChar->GetRootComponent());
	}

	return AC;
}

void AGun::StopWeaponAnimation(const FWeaponAnim& Animation)
{
	if (OwningChar)
	{
		UAnimMontage* UseAnim = OwningChar->IsFirstPerson() ? Animation.Pawn1P : Animation.Pawn3P;
		if (UseAnim)
		{
			OwningChar->StopAnimMontage(UseAnim);
		}
	}
}

float AGun::PlayWeaponAnimation(const FWeaponAnim& Animation)
{
	float Duration = 0.0f;
	if (OwningChar)
	{
		UAnimMontage* UseAnim = OwningChar->IsFirstPerson() ? Animation.Pawn1P : Animation.Pawn3P;
		if (UseAnim)
		{
			Duration = OwningChar->PlayAnimMontage(UseAnim);
		}
	}

	return Duration;
}

//////////////////////////////////////////////////
//Weapon Usage Helpers
//////////////////////////////////////////////////

FVector AGun::GetAdjustedAim() const
{
	APlayerController* const PlayerController = Instigator ? Cast<APlayerController>(Instigator->Controller) : nullptr;
	FVector FinalAim = FVector::ZeroVector;
	// If we have a player controller use it for the aim
	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}
	else if (Instigator)
	{
		// Now see if we have an AI controller - we will want to get the aim from there if we do
		AAIController* AIController = OwningChar ? Cast<AAIController>(OwningChar->Controller) : nullptr;
		if (AIController)
		{
			FinalAim = AIController->GetControlRotation().Vector();
		}
		else
		{
			FinalAim = Instigator->GetBaseAimRotation().Vector();
		}
	}

	return FinalAim;
}

FVector AGun::GetCameraAim() const
{
	APlayerController* const PlayerController = Instigator ? Cast<APlayerController>(Instigator->Controller) : nullptr;
	FVector FinalAim = FVector::ZeroVector;

	if (PlayerController)
	{
		FVector CamLoc;
		FRotator CamRot;
		PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
		FinalAim = CamRot.Vector();
	}
	else if (Instigator)
	{
		FinalAim = Instigator->GetBaseAimRotation().Vector();
	}

	return FinalAim;
}

FVector AGun::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	APlayerController* PC = OwningChar ? Cast<APlayerController>(OwningChar->Controller) : nullptr;
	AAIController* AIPC = OwningChar ? Cast<AAIController>(OwningChar->Controller) : nullptr;
	FVector OutStartTrace = FVector::ZeroVector;
	if (PC)
	{
		// use player's camera
		FRotator UnusedRot;
		PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);

		// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
		OutStartTrace = OutStartTrace + AimDir * ((Instigator->GetActorLocation() - OutStartTrace) | AimDir);
	}
	else if (AIPC)
	{
		OutStartTrace = GetMuzzleLocation();
	}

	return OutStartTrace;
}


FHitResult AGun::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{
	static FName WeaponFireTag = FName(TEXT("WeaponTrace"));

	// Perform trace to retrieve hit info
	FCollisionQueryParams TraceParams(WeaponFireTag, true, Instigator);
	TraceParams.bTraceAsyncScene = true;
	TraceParams.bReturnPhysicalMaterial = true;

	FHitResult Hit(ForceInit);
	//Be sure to change ECC_GameTraceChannel to correct channel for Weapon
	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Weapon, TraceParams);

	return Hit;
}