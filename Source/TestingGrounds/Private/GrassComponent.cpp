// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "GrassComponent.h"


UGrassComponent::UGrassComponent()
{
	SetCollisionProfileName(TEXT("NoCollision"));
}

void UGrassComponent::SpawnGrass(const FBox& SpawnExtents)
{
	for (int32 i = 0; i < SpawnCount; ++i)
	{
		FVector Location = FMath::RandPointInBox(SpawnExtents);
		AddInstance(FTransform(Location));
	}
}