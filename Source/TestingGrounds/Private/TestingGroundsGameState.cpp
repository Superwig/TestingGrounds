// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "TestingGroundsGameState.h"

ATestingGroundsGameState::ATestingGroundsGameState()
{
	TilesConquered = 0;
}

int32 ATestingGroundsGameState::GetTilesConquered() const
{
	return TilesConquered;
}

void ATestingGroundsGameState::IncrementTilesConquered()
{
	++TilesConquered;
	TileCountUpdated.Broadcast();
}