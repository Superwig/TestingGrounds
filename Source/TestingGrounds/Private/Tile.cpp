// Fill out your copyright notice in the Description page of Project Settings.

#include "TestingGrounds.h"
#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "AI/Navigation/NavMeshBoundsVolume.h"
#include "DrawDebugHelpers.h"
#include "GrassComponent.h"
#include "Builders/CubeBuilder.h"
#include "TestingGroundsGameState.h"


// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	SharedRoot = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	BoundsVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Bounds Volume"));	
	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow Component"));
	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	Grass01 = CreateDefaultSubobject<UGrassComponent>(TEXT("Grass01"));
	Grass02 = CreateDefaultSubobject<UGrassComponent>(TEXT("Grass02"));
	Grass03 = CreateDefaultSubobject<UGrassComponent>(TEXT("Grass03"));

	Floor->SetCollisionResponseToChannel(ECC_Ground, ECR_Block);
	Floor->SetCollisionResponseToChannel(ECC_Spawn, ECR_Ignore);

	RootComponent = SharedRoot;
	BoundsVolume->SetupAttachment(RootComponent);
	AttachPoint->SetupAttachment(RootComponent);
	Floor->SetupAttachment(RootComponent);
	Grass01->SetupAttachment(Floor);
	Grass02->SetupAttachment(Floor);
	Grass03->SetupAttachment(Floor);

	bConquered = false;

	BoundsVolume->SetCollisionProfileName(TEXT("Trigger"));

	PlayerEntered.AddDynamic(this, &ATile::OnEnter);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	check(Barrier)
	check(BarrierMaterial)
	Super::BeginPlay();
	DynamicMaterial = Barrier->CreateAndSetMaterialInstanceDynamicFromMaterial(0, BarrierMaterial);

	BoundsVolume->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnBoundsBeginOverlap);
	BoundsVolume->OnComponentEndOverlap.AddDynamic(this, &ATile::OnBoundsEndOverlap);

	FVector Min;
	FVector Max;
	Floor->GetLocalBounds(Min, Max);
	Min.Z = Max.Z;
	ActorToWorld().TransformPosition(Min);
	ActorToWorld().TransformPosition(Max);
	FBox Bounds(Min, Max);
	Grass01->SpawnGrass(Bounds);
	Grass02->SpawnGrass(Bounds);
	Grass03->SpawnGrass(Bounds);
}

FTransform ATile::GetAttachLocation() const
{
	return AttachPoint->ComponentToWorld;
}

TArray<AActor*> ATile::GetSpawnedActors() const 
{
	return SpawnedActors;
}

void ATile::SetNavVolume(ANavMeshBoundsVolume* Volume)
{
	NavVolume = Volume;
	NavVolume->SetActorLocation(GetComponentsBoundingBox(false).GetCenter());
	GetWorld()->GetNavigationSystem()->Build();

}

ANavMeshBoundsVolume* ATile::GetNavVolume() const 
{
	return NavVolume;
}

void ATile::OnBoundsBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AActor* Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (OtherActor == Player)
	{
		Barrier->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		PlayerEntered.Broadcast();
	}
}

void ATile::OnBoundsEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AActor* Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (OtherActor == Player)
	{
		Barrier->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		ATestingGroundsGameState* GameState = Cast<ATestingGroundsGameState>(GetWorld()->GetGameState());
		if (!bConquered)
		{
			bConquered = true;
			GameState->IncrementTilesConquered();
		}
		OnExit();
	}
}

void ATile::UpdateBarrierMaterial(float Blend, float Opacity)
{
	DynamicMaterial->SetScalarParameterValue(TEXT("Blend"), FMath::Clamp(Blend, 0.f, 1.f));
	DynamicMaterial->SetScalarParameterValue(TEXT("Opacity"), FMath::Clamp(Opacity, .5f, 1.f));
}

void ATile::SpawnAIInTile(TSubclassOf<APawn> SpawnActor, float Radius, bool bOnlyYaw, int32 MinSpawn, int32 MaxSpawn)
{
	SpawnActorsInTile(SpawnActor, Radius, bOnlyYaw, MinSpawn, MaxSpawn, 1.f, 1.f);
}

void ATile::SpawnActorsInTile(TSubclassOf<AActor> SpawnActor, float Radius, bool bOnlyYaw, int32 MinSpawn, int32 MaxSpawn, float MinScale, float MaxScale)
{
	int32 NumSpawn = FMath::RandRange(MinSpawn, MaxSpawn);
	constexpr int32 Attempts = 1000;
	for (int32 AttemptCount = 0, SpawnCount = 0; SpawnCount < NumSpawn && AttemptCount < Attempts; ++AttemptCount)
	{
		FBox BoundingBox = GetComponentsBoundingBox(false);
		FVector Point = FMath::RandPointInBox(BoundingBox);
		FVector EndTrace = Point + (Point.UpVector * -500.f);
		FHitResult Hit(ForceInit);
		GetWorld()->LineTraceSingleByChannel(Hit, Point, EndTrace, ECC_Ground);
		if (Hit.GetActor())
		{
			float Scale = FMath::RandRange(MinScale, MaxScale);
			FRotator SpawnRot(ForceInit);
			if (bOnlyYaw)
			{
				float Rot = FMath::RandRange(180.f, -180.f);
				SpawnRot.Yaw = Rot;
			}
			else
			{
				float X = FMath::RandRange(180.f, -180.f);
				float Y = FMath::RandRange(180.f, -180.f);
				float Z = FMath::RandRange(180.f, -180.f);
				SpawnRot = FRotator(X, Y, Z);
			}
			FVector SpawnPoint = Hit.ImpactPoint;
			//to make sweep detect collisions if inside an actor
			FVector SpawnPoint2 = SpawnPoint;
			SpawnPoint2.Z += .1f;

			bool bHasCollided = GetWorld()->SweepSingleByChannel(Hit, SpawnPoint, SpawnPoint2, FQuat::Identity, ECC_Spawn, FCollisionShape::MakeSphere(Radius * Scale));
			if (!bHasCollided)
			{
				AActor* SpawnedActor = GetWorld()->SpawnActor<AActor>(SpawnActor, FTransform(SpawnRot, SpawnPoint, FVector(Scale)));
				if (SpawnedActor)
				{
					//TODO for some reason not taken into account in FTransform
					SpawnedActor->SetActorScale3D(FVector(Scale));
					SpawnedActors.Add(SpawnedActor);
					SpawnedActor->OnDestroyed.AddDynamic(this, &ATile::OnSpawnedActorDestroyed);
					APawn* SpawnedPawn = Cast<APawn>(SpawnedActor);
					if (SpawnedPawn)
					{
						SpawnedPawn->SpawnDefaultController();
						SpawnedPawn->Tags.Add(TEXT("Guard"));
					}
					++SpawnCount;
				}
			}
		}
	}
}

void ATile::OnSpawnedActorDestroyed(AActor* DestroyedActor)
{
	SpawnedActors.RemoveAll([DestroyedActor](AActor* Actor) { return Actor == DestroyedActor; });
}