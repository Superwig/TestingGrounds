// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "TestingGrounds.h"

#include "TestingGroundsGameMode.h"
#include "TestingGroundsHUD.h"
#include "Tile.h"
#include "TestingGroundsGameState.h"
#include "ShooterPlayerState.h"

#include "EngineUtils.h"
#include "AI/Navigation/NavMeshBoundsVolume.h"

ATestingGroundsGameMode::ATestingGroundsGameMode()
	: Super()
{
	FVector Location{ -2000.0, 0, 120 };
	AttachLocation = FTransform(FRotator(0),Location);
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Dynamic/Character/BP_Character"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
	PlayerStateClass = AShooterPlayerState::StaticClass();
	GameStateClass = ATestingGroundsGameState::StaticClass();
	// use our custom HUD class
	HUDClass = ATestingGroundsHUD::StaticClass();
}

void ATestingGroundsGameMode::BeginPlay()
{
	check(TileClass)
	//If MaxTiles overidden in BP
	if (MaxTiles != DEFAULT_MAX)
	{
		Tiles = TCircularQueue<ATile*>(MaxTiles);
		NavVolumes = TCircularQueue<ANavMeshBoundsVolume*>(MaxTiles);
	}
	//Populate NavVolumes
	for (TActorIterator<ANavMeshBoundsVolume>NavIter(GetWorld()); NavIter; ++NavIter)
	{
		NavVolumes.Enqueue(*NavIter);
	}
	//Spawn
	SpawnTile();
	SpawnTile();
	//Only call BP BeginPlay after initial setup
	Super::BeginPlay();
}

void ATestingGroundsGameMode::SpawnTile()
{
	if (GetWorld())
	{
		ATile* NewTile = GetWorld()->SpawnActor<ATile>(TileClass, AttachLocation);
		if (NewTile)
		{
			//Dequeue if at MaxTiles
			if (Tiles.Count() >= MaxTiles)
			{
				ATile* OldTile;
				if (Tiles.Dequeue(OldTile))
				{
					//Re-Queue NavVolume
					NavVolumes.Enqueue(OldTile->GetNavVolume());
					//Cleanup and destroy OldTile
					for (AActor* Actor : OldTile->GetSpawnedActors())
					{
						//check to see if it's still alive
						if (Actor)
						{
							Actor->SetLifeSpan(0.3f);
						}
					}
					OldTile->SetLifeSpan(0.3f);
				}
			}
			//Bind NewTile's BoundsVolume OnOverlap with SpawnTile
			NewTile->PlayerEntered.AddDynamic(this, &ATestingGroundsGameMode::SpawnTile);
			//Get new AttachLocation then Queue new Tile
			AttachLocation = NewTile->GetAttachLocation();
			Tiles.Enqueue(NewTile);
			//Dequeue a NavVolume
			ANavMeshBoundsVolume* Volume;
			if (NavVolumes.Dequeue(Volume))
			{
				NewTile->SetNavVolume(Volume);
			}

		}
	}

}